# OrangeFox for nash device tree

### Device specifications
=====================================

Basic   | Spec Sheet
-------:|:-------------------------
CPU     | Octa-core (4x2.45 GHz Kryo & 4x1.9 GHz Kryo)
CHIPSET | Qualcomm MSM8998 Snapdragon 835
GPU     | Adreno 540
Memory  | 4/6GB
Shipped Android Version | 7.1 (Nougat)
Storage | 64/128GB
Battery | 2730 mAh
Dimensions | 155.8 x 76 x 6.1 mm
Display | 1440 x 2560 pixels/5.5" P-OLED
Rear Camera  | Dual 12 MP
Front Camera | 5 MP
