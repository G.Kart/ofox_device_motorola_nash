#!/bin/bash

# configure some default settings for the build
Default_Settings() {
    export TARGET_ARCH=arm64
    export OF_MAINTAINER="GKart"
    export FOX_VERSION=R11.1
    export FOX_BUILD_TYPE="Beta"
    export FOX_USE_TWRP_RECOVERY_IMAGE_BUILDER=1
    export ALLOW_MISSING_DEPENDENCIES=true
    export LC_ALL="C"
    export OF_SUPPORT_ALL_BLOCK_OTA_UPDATES=0
    export OF_DISABLE_MIUI_SPECIFIC_FEATURES=1
    export OF_DISABLE_MIUI_OTA_BY_DEFAULT=1
    export FOX_ADVANCED_SECURITY=1
    export OF_FLASHLIGHT_DISABLE=1
    export FOX_USE_NANO_EDITOR=1
    export OF_FORCE_DISABLE_DM_VERITY=1
    export OF_FORCE_DISABLE_DM_VERITY_FORCED_ENCRYPTION=1
    export OF_DISABLE_DM_VERITY=1
    export OF_DISABLE_FORCED_ENCRYPTION=1
    export OF_DISABLE_DM_VERITY_FORCED_ENCRYPTION=1
}

# build the project
do_build() {
  Default_Settings

  # compile it
  . build/envsetup.sh
  
  lunch omni_nash-eng
  
  mka recoveryimage -j`nproc`
}

# --- main --- #
do_build
#
